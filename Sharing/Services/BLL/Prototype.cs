﻿using Sharing.Repository.Models.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sharing.Services.BLL
{
    interface Prototype <Tin, Tout>
    {
        bool isValid(int input);
        List<Tout> AllRecords();
        List<Tout> IntersectSomeRecords(List<Filter> filters);
        List<Tout> UnionAllSomeRecords(List<Filter> filters);
        Tout Insert(Tin input);
        Tout Update(Tin input);
        Tout Delete(Tin input);
    }
}
