﻿using Sharing.Repository.Models.Input;
using Sharing.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sharing.Services.BLL
{
    public class Post : Prototype<Repository.Models.SqlServer.Post, Repository.Models.View.Post>
    {
        private Repository.Models.View.Post ToView(Repository.Models.SqlServer.Post input)
        {
            return new Repository.Models.View.Post()
            {
                WriterId = input.WriterId,
                Submit = input.Submit.ToShortDateString(),
                Title = input.Title,
                Describe = input.Describe,
                Content = input.Content
            };
        }

        private List<Repository.Models.View.Post> ToView(List<Repository.Models.SqlServer.Post> input)
        {
            List<Repository.Models.View.Post> result = new List<Repository.Models.View.Post>();

            foreach (Repository.Models.View.Post assistance in input)
                result.Add(ToView(assistance));

            return result;
        }

        public bool isValid(int input)
        {
            throw new NotImplementedException();
        }

        public List<Repository.Models.View.Post> AllRecords()
        {
            using (var help = new SharingDbContext())
                return ToView(help.Posts.ToList());
        }

        public List<Repository.Models.View.Post> IntersectSomeRecords(List<Filter> filters = null)
        {
            if (filters is null)
                return new List<Repository.Models.View.Post>();
            List<List<Repository.Models.SqlServer.Post>> asssistances = new List<List<Repository.Models.SqlServer.Post>>();
            List<Repository.Models.View.Post> result = new List<Repository.Models.View.Post>();
            int jailer = 0;
            List<Repository.Models.SqlServer.Post> beforeAssistance = new List<Repository.Models.SqlServer.Post>();

            using (var help = new SharingDbContext())
                foreach (Filter filter in filters)
                {
                    switch (filter.Field.ToLower())
                    {
                        case "id":
                            asssistances.Add(help.Posts.Where(x => x.Id.ToString() == filter.ToNative()).ToList());
                            break;
                        case "writerid":
                            asssistances.Add(help.Posts.Where(x => x.WriterId.ToString() == filter.ToNative()).ToList());
                            break;
                        case "submit":
                            asssistances.Add(help.Posts.Where(x => x.Submit.ToShortDateString() == filter.ToNative()).ToList());
                            break;
                        case "title":
                            asssistances.Add(help.Posts.Where(x => x.Title == filter.ToNative()).ToList());
                            break;
                        case "describe":
                            asssistances.Add(help.Posts.Where(x => x.Describe == filter.ToNative()).ToList());
                            break;
                        case "content":
                            asssistances.Add(help.Posts.Where(x => x.Content == filter.ToNative()).ToList());
                            break;
                        default:
                            asssistances.Add(new List<Repository.Models.SqlServer.Post>());
                            break;
                    }

                    beforeAssistance.AddRange(asssistances[jailer]);

                    if (jailer != 0)
                        help.Posts.Intersect(beforeAssistance);

                    beforeAssistance = asssistances[jailer];
                    jailer++;
                }

            return result;
        }

        public List<Repository.Models.View.Post> UnionAllSomeRecords(List<Filter> filters)
        {
            throw new NotImplementedException();
        }

        public Repository.Models.View.Post Insert(Repository.Models.SqlServer.Post input)
        {
            throw new NotImplementedException();
        }

        public Repository.Models.View.Post Update(Repository.Models.SqlServer.Post input)
        {
            throw new NotImplementedException();
        }

        public Repository.Models.View.Post Delete(Repository.Models.SqlServer.Post input)
        {
            throw new NotImplementedException();
        }
    }
}
