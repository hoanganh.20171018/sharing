﻿using Sharing.Repository.Models.Input;
using Sharing.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sharing.Services.BLL
{
    public class Writer : Prototype<Repository.Models.SqlServer.Writer, Repository.Models.View.Writer>
    {
        private Repository.Models.View.Writer ToView(Repository.Models.SqlServer.Writer input)
        {
            return new Repository.Models.View.Writer() {
                UserName = input.UserName,
                Pwd = "",
                Name = input.Name,
                Dob = input.Dob.ToShortDateString(),
                Address = input.Address,
                Email = input.Email,
                PhoneNumber = input.PhoneNumber
            };
        }

        private List<Repository.Models.View.Writer> ToView(List<Repository.Models.SqlServer.Writer> inputs)
        {
            List<Repository.Models.View.Writer> result = new List<Repository.Models.View.Writer>();

            foreach (var input in inputs)
                result.Add(ToView(input));

            return result;
        }

        public bool isValid(int inputId)
        {
            using (var help = new SharingDbContext())
                return help.Writers.Where(writer => writer.Id == inputId).Count() == 1;
        }
        public bool isValid(string inputUser)
        {
            using (var help = new SharingDbContext())
                return help.Writers.Where(writer => writer.UserName == inputUser).Count() == 1;
        }

        public List<Repository.Models.View.Writer> AllRecords()
        {
            throw new NotImplementedException();
        }

        public List<Repository.Models.View.Writer> IntersectSomeRecords(List<Filter> filters)
        {
            throw new NotImplementedException();
        }

        public List<Repository.Models.View.Writer> UnionAllSomeRecords(List<Filter> filters)
        {
            throw new NotImplementedException();
        }

        public Repository.Models.View.Writer Insert(Repository.Models.SqlServer.Writer input)
        {
            input.Pwd = Encode.ToString(input.Pwd);

            using (var help = new SharingDbContext())
            {
                help.Writers.Add(input);
                help.SaveChanges();
            }

            return ToView(input);
        }

        public Repository.Models.View.Writer Update(Repository.Models.SqlServer.Writer input)
        {
            throw new NotImplementedException();
        }

        public Repository.Models.View.Writer Delete(Repository.Models.SqlServer.Writer input)
        {
            throw new NotImplementedException();
        }

        public Repository.Models.View.Writer Login(Repository.Models.SqlServer.Writer input)
        {
            input.Pwd = Encode.ToString(input.Pwd);

            using (var help = new SharingDbContext())
            {
                if (help.Writers.Where(x => x.UserName == input.UserName && x.Pwd == input.Pwd).ToList().Count == 1)
                    return ToView(input);

                return new Repository.Models.View.Writer();
            }
        }
    }
}
