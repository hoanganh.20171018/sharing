﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Buffers.Text;
using System.Text;
using System.Security.Cryptography;

namespace Sharing.Services
{
    public class Encode
    {
        public static string ToString(string input)
        {
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            
            byte[] hash = MD5.Create().ComputeHash(inputBytes);
            
            StringBuilder result = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }

            return result.ToString();
        }
    }
}
