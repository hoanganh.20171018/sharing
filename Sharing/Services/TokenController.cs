﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Sharing.Repository;

namespace Sharing.Services
{
    public class TokenController : Controller
    {
        private IConfiguration configuration;

        private string BuildToken() =>
            new JwtSecurityTokenHandler().WriteToken(
                new JwtSecurityToken(configuration["Jwt:Issuer"],
                    configuration["Jwt:Issuer"],
                    expires: DateTime.Now.AddMinutes(60),
                    signingCredentials: new SigningCredentials(
                        new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(configuration["Jwt:Key"])),
                            SecurityAlgorithms.HmacSha256
                        )
                    )
                );

        private Repository.Models.View.User Authenticate(Repository.Models.Input.Login input)
        {
            Repository.Models.View.User result = null;

            using (var help = new SharingDbContext())
            {
                List<Repository.Models.SqlServer.Writer> writers = help.Writers.Where(x => x.UserName == input.User && x.Pwd == Encode.ToString(input.Pwd)).ToList();

                if (writers.Count == 1)
                    result = new Repository.Models.View.User() {
                        Name = writers.First().Name,
                        Dob = writers.First().Dob,
                        Address = writers.First().Address,
                        Email = writers.First().Email,
                        PhoneNumber = writers.First().PhoneNumber
                    };
            }

            return result;
        }

        public TokenController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult CreateToken(Repository.Models.Input.Login input)
        {
            if (Authenticate(input) is not null)
                return Ok(new { token = BuildToken() });

            return Ok(Unauthorized());
        }
    }
}
