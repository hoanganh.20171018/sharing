﻿using Microsoft.AspNetCore.Mvc;
using Sharing.Repository.Models.Input;
using System;
using System.Collections.Generic;
using Sharing.Repository.Models.View;
using Sharing.Services;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;

namespace Sharing.Controllers
{
    [ApiController]
    [Route("Writer")]
    public class WriterController : Controller
    {
        private IConfiguration configuration;

        public WriterController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        [HttpPost("Insert")]
        public IActionResult Insert(Repository.Models.SqlServer.Writer input)
        {
            if (new Services.BLL.Writer().isValid(input.UserName))
                return Ok(View<Writer>
                        .False(null, "Đã Tồn Tại Tài Khoản", null));

            Writer data = null;

            try
            {
                data = new Services.BLL.Writer().Insert(input);
                    
            }
            catch (Exception exception)
            {
                return Ok(View<Writer>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<Repository.Models.SqlServer.Writer>
                .True(data));
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public IActionResult Login(Login input)
        {
            return new TokenController(configuration).CreateToken(input);
        }
        /*
        public IActionResult Update(Repository.Models.SqlServer.Writer input)
        {
            throw new NotImplementedException();
        }

        public IActionResult Delete(Repository.Models.SqlServer.Writer input)
        {
            throw new NotImplementedException();
        }*/
    }
}
