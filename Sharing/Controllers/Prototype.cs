﻿using Microsoft.AspNetCore.Mvc;
using Sharing.Repository.Models.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sharing.Controllers
{
    interface Prototype<T>
    {
        IActionResult AllRecords();
        IActionResult SomeRecords(List<Filter> filters);
        IActionResult Insert(T input);
        IActionResult Update(T input);
        IActionResult Delete(T input);
    }
}
