﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Sharing.Repository.Models.SqlServer
{
    [Table("Post")]
    public partial class Post
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("writerId")]
        public int WriterId { get; set; }
        [Column("submit", TypeName = "date")]
        public DateTime Submit { get; set; }
        [Required]
        [Column("title")]
        [StringLength(128)]
        public string Title { get; set; }
        [Column("describe")]
        [StringLength(256)]
        public string Describe { get; set; }
        [Required]
        [Column("content")]
        [StringLength(2048)]
        public string Content { get; set; }

        [ForeignKey(nameof(WriterId))]
        [InverseProperty("Posts")]
        public virtual Writer Writer { get; set; }
    }
}
