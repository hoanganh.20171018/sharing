﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Sharing.Repository.Models.SqlServer
{
    [Table("Writer")]
    public partial class Writer
    {
        public Writer()
        {
            Posts = new HashSet<Post>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Required]
        [Column("userName")]
        [StringLength(64)]
        public string UserName { get; set; }
        [Required]
        [Column("pwd")]
        [StringLength(64)]
        public string Pwd { get; set; }
        [Required]
        [Column("_name")]
        [StringLength(128)]
        public string Name { get; set; }
        [Column("dob", TypeName = "date")]
        public DateTime Dob { get; set; }
        [Column("_address")]
        [StringLength(256)]
        public string Address { get; set; }
        [Required]
        [Column("email")]
        [StringLength(64)]
        public string Email { get; set; }
        [Required]
        [Column("phoneNumber")]
        [StringLength(16)]
        public string PhoneNumber { get; set; }

        [InverseProperty(nameof(Post.Writer))]
        public virtual ICollection<Post> Posts { get; set; }
    }
}
