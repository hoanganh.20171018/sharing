﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sharing.Repository.Models.View
{
    public class Writer : SqlServer.Writer
    {
        public new string Dob { get; set; }
        public new string Pwd { get; set; }
    }
}
