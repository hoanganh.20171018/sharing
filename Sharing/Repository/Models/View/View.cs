﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sharing.Repository.Models.View
{
    public class View<T>
    {
        public T Data { get; set; }
        public bool State { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }
        public string ExceptionMessage => Exception?.Message;

        public View()
        {

        }
        public View(T Data, bool State, string Message, Exception Exception)
        {
            this.Data = Data;
            this.State = State;
            this.Message = Message;
            this.Exception = Exception;
        }

        public static View<T> True(T Data, string Message = null)
        {
            return new View<T>(Data, true, $"{Message}", null);
        }
        public static View<T> False(T Data, string Message, Exception Exception)
        {
            return new View<T>(Data, false, Message, Exception);
        }
    }
}
