﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sharing.Repository.Models.Input
{
    public class Login
    {
        public string User { get; set; }
        public string Pwd { get; set; }
    }
}
