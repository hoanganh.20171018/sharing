﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sharing.Repository.Models.Input
{
    public class Filter
    {
        public string Field { get; set; }
        public object Value { get; set; }

        public string ToNative()
        {
            if (Value is int intValue)
                return "" + intValue;
            else if (Value is DateTime dateValue)
                return $"'{dateValue.ToShortDateString()}'";
            else if (Value is string stringValue)
                return $"'{stringValue}'";

            return "";
        }
    }
}
